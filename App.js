import React, { Component } from 'react';
import {Linking} from 'react-native';
import {
  StyleSheet,
  AppRegistry,
  ImageBackground,
  Text,
  ScrollView,
  View,
} from 'react-native';

export default class BackgroundImage extends Component {
  state = {
    location: null,
    temperature: null,
    humidity: null,
    description: null,
    lat: null,
    lon: null,
    place1: null,
    place2: null,
    place3: null,
    place4: null,
    place5: null,
    placeAddress1: null,
    placeAddress2: null,
    placeAddress3: null,
    placeAddress4: null,
    placeAddress5: null,
  };

  weather = {"latitude":21.0313,"longitude":105.8516,"city":"X\u00f3m Pho","weather":{"simple":"Clouds","description":"overcast clouds","temperature":18},"source":"smart-ip.net, openweathermap.org"};
  dataPlaces = {
   "html_attributions" : [],
   "next_page_token" : "CpQCDAEAAO5s4J7GtX_wD_90aQUlYx-uqA2-k_V-qIYCtgv4EBiiYJOo-43Z7SUgam5GtUs5TovA113TKL7O-gvb3WW6yTcik7oZH9-JzSG9sWPTqIp5zW2z1NIAg6laKRAr47QQeGeoRtlobxqS698uSObknKzvxaPTOUVW02yGXJTVrD2OlvgvjewjsLd1Ki8oQHuoTAIC4WvfEcw2XPccFlqVj6Sht1sJhMzm3SIM1ue65mqhMBhzJUPhEsVld9EOdVF0Wj8Mfy3Dd1A7t7fpm7WmUjWPqdhlARP2l9w8He-BAukVUdRTgtWk3hPmQdTUBliQ7oMpV_MWYkEBDYTrbbRHZW8yDg40mvPhUOH-rLjXQxjcEhBv-NIDhrLqj_omstMZ4LrzGhQFCx9Z0ailEjM8SZNpDY2Yz1DLhw",
   "results" : [
      {
         "formatted_address" : "Hàng Ngang, Hàng Đào, Hoàn Kiếm, Hà Nội 100000, Vietnam",
         "geometry" : {
            "location" : {
               "lat" : 21.034059,
               "lng" : 105.8506368
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 21.03539657989272,
                  "lng" : 105.8519573298927
               },
               "southwest" : {
                  "lat" : 21.03269692010728,
                  "lng" : 105.8492576701073
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png",
         "id" : "a3ae5c74b656b8444f482bc41076122f741e4b5c",
         "name" : "Old Quarter",
         "opening_hours" : {
            "open_now" : true
         },
         "photos" : [
            {
               "height" : 1322,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/103021600982132188101\"\u003eA Google User\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAXUwzZyLFxhs4sBXoKgxx4y1SHvQvbM4rDcleTDdCNhUy6ja4ojrnscMDvKGGfp-CLDLzBRTVe57CjoQwVwYmCjRt4pJkPuT6sZecJzi-Z-RsyNAX946msf5rDRPhPz5JEhCh80BdUSSGN3mw4d4RyAn0GhRb8knZgJcsgCHBiObjilQ9-Jtz2Q",
               "width" : 1980
            }
         ],
         "place_id" : "ChIJp0o4Er6rNTERjlTif_IXU1k",
         "plus_code" : {
            "compound_code" : "2VM2+J7 Hàng Đào, Hoàn Kiếm, Hanoi",
            "global_code" : "7PH72VM2+J7"
         },
         "rating" : 4.5,
         "reference" : "ChIJp0o4Er6rNTERjlTif_IXU1k",
         "types" : [ "tourist_attraction", "point_of_interest", "establishment" ],
         "user_ratings_total" : 2373
      },
      {
         "formatted_address" : "Đinh Tiên Hoàng, Hàng Trống, Hoàn Kiếm, Hà Nội 100000, Vietnam",
         "geometry" : {
            "location" : {
               "lat" : 21.0307282,
               "lng" : 105.8523393
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 21.03204857989272,
                  "lng" : 105.8543438298927
               },
               "southwest" : {
                  "lat" : 21.02934892010728,
                  "lng" : 105.8516441701073
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png",
         "id" : "9b36ee6d8d8a438f706050e185eca891c56dfb41",
         "name" : "Temple of the Jade Mountain",
         "opening_hours" : {
            "open_now" : true
         },
         "photos" : [
            {
               "height" : 2448,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/112806673945904401677\"\u003e김종현\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAYj5daJFI-qrUa5nk1LrLA_dAakk_C7iEm8PE7jwFNfQSxuoDdlnl03eSNbTEkTdIqEuM_TF7FT40ryayfupSa280Vu2CJYdXrzw3SS3IHXfL5k33iCLsQc9_FQUyAxKzEhCU7RV4Eg_Q36zBR3sAIMjUGhTJ2tO4X1NhhxjG11YE55ibhmsLZg",
               "width" : 3264
            }
         ],
         "place_id" : "ChIJp56AAMCrNTERHTeCAuwWJ7s",
         "plus_code" : {
            "compound_code" : "2VJ2+7W Hanoi, Hoàn Kiếm, Hanoi",
            "global_code" : "7PH72VJ2+7W"
         },
         "rating" : 4.3,
         "reference" : "ChIJp56AAMCrNTERHTeCAuwWJ7s",
         "types" : [
            "tourist_attraction",
            "place_of_worship",
            "point_of_interest",
            "establishment"
         ],
         "user_ratings_total" : 5429
      },
      {
         "formatted_address" : "19C Hoàng Diệu, Điện Bàn, Ba Đình, Hà Nội 100000, Vietnam",
         "geometry" : {
            "location" : {
               "lat" : 21.034381,
               "lng" : 105.8401142
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 21.03578232989272,
                  "lng" : 105.8410158798927
               },
               "southwest" : {
                  "lat" : 21.03308267010728,
                  "lng" : 105.8383162201073
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png",
         "id" : "3bb4628a3ad57e09d79f9658836f9411032a5eac",
         "name" : "Thăng Long Imperial Citadel",
         "opening_hours" : {
            "open_now" : true
         },
         "photos" : [
            {
               "height" : 1367,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/104480692753122120835\"\u003eJames Tseng\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAA6bb4dpbimflWteWCw9ctI3P52NWyzDduwQ_Sc8x_rQucIwm9dTdWySPGc96vaHGz9KFHzJSbiP1UYywkymAjr2zcGIfz6Of0jZeET9R_-LMHQUkqS2SsJl1k4M3pZUFrEhDHWOWHZIVWMRzK5lqjoVzxGhQN-JyOgPZEV4HSNLBwKXh-69Zfzw",
               "width" : 2048
            }
         ],
         "place_id" : "ChIJSXwdOKOrNTERNylYj9mnIbU",
         "plus_code" : {
            "compound_code" : "2RMR+Q2 Điện Bàn, Ba Đình, Hanoi",
            "global_code" : "7PH72RMR+Q2"
         },
         "rating" : 4.3,
         "reference" : "ChIJSXwdOKOrNTERNylYj9mnIbU",
         "types" : [ "tourist_attraction", "point_of_interest", "establishment" ],
         "user_ratings_total" : 8170
      },
      {
         "formatted_address" : "Số 01 Tràng Tiền, Phan Chu Trinh, Hoàn Kiếm, Hà Nội 100000, Vietnam",
         "geometry" : {
            "location" : {
               "lat" : 21.024184,
               "lng" : 105.857857
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 21.02553942989272,
                  "lng" : 105.8590857298927
               },
               "southwest" : {
                  "lat" : 21.02283977010728,
                  "lng" : 105.8563860701073
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png",
         "id" : "e8f042b7f5e88d3a684ef371a193d85c21a823db",
         "name" : "Hanoi Opera House",
         "opening_hours" : {
            "open_now" : true
         },
         "photos" : [
            {
               "height" : 3000,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/114971592884911893585\"\u003ePham Thanh\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAArgdDvANq-UetUqrOFTyJQGXqxSy7-4N-a4SL4vlG-FFafoiWt-ykaQ1EvZVBznZQ9YEKskOFSSrSwz2FsXVFIfi1s86p0Ehey-jkYN1XcgYftN3ftWRC5g7HdwFHCsmSEhAD0Tp7mshyyEX5qQZEAZ4eGhS4_aPmkGaDclEfaNQC3FiND-BLKA",
               "width" : 5333
            }
         ],
         "place_id" : "ChIJEQB--OurNTERK41Q2gDyemQ",
         "plus_code" : {
            "compound_code" : "2VF5+M4 Phan Chu Trinh, Hoàn Kiếm, Hanoi",
            "global_code" : "7PH72VF5+M4"
         },
         "rating" : 4.5,
         "reference" : "ChIJEQB--OurNTERK41Q2gDyemQ",
         "types" : [ "tourist_attraction", "point_of_interest", "establishment" ],
         "user_ratings_total" : 10518
      },
      {
         "formatted_address" : "57B Đinh Tiên Hoàng, Hàng Bạc, Hoàn Kiếm, Hà Nội 100000, Vietnam",
         "geometry" : {
            "location" : {
               "lat" : 21.0317018,
               "lng" : 105.8533415
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 21.03305912989272,
                  "lng" : 105.8545886798927
               },
               "southwest" : {
                  "lat" : 21.03035947010728,
                  "lng" : 105.8518890201073
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png",
         "id" : "27177e30558a337bc51c17c1ee7a149de0fa5bfb",
         "name" : "Thang Long Water Puppet Theatre",
         "opening_hours" : {
            "open_now" : true
         },
         "photos" : [
            {
               "height" : 1365,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/105359571720879171629\"\u003eKern LIM\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAtuM9N0plB3Y6cuZV11reH1mMPrySGiovWODd-uJkfZ3rO79nDaXRAhCC3yFgC6jnfIxVVLbcLooJNEUZPT_tjwt6Kd9sjCzJbCQiPp2R1ZS2kzM1rBpAqEABf3aM2tPEEhADwBf2QllkuTwsFkiT6xtEGhTKK0RZzIZLDlHTk2_Mu9pXnkJYgw",
               "width" : 2048
            }
         ],
         "place_id" : "ChIJiUJFE8CrNTERHK060qWnXk4",
         "plus_code" : {
            "compound_code" : "2VJ3+M8 Hàng Bạc, Hoàn Kiếm, Hanoi",
            "global_code" : "7PH72VJ3+M8"
         },
         "rating" : 4.2,
         "reference" : "ChIJiUJFE8CrNTERHK060qWnXk4",
         "types" : [ "tourist_attraction", "point_of_interest", "establishment" ],
         "user_ratings_total" : 8673
      },
      {
         "formatted_address" : "1 Hoả Lò, Trần Hưng Đạo, Hoàn Kiếm, Hà Nội 100000, Vietnam",
         "geometry" : {
            "location" : {
               "lat" : 21.025235,
               "lng" : 105.8463895
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 21.02655772989272,
                  "lng" : 105.8478406298927
               },
               "southwest" : {
                  "lat" : 21.02385807010728,
                  "lng" : 105.8451409701073
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png",
         "id" : "2091e18e20fbdf7e7f743ad5a24826ada56d1298",
         "name" : "Hoa Lo Prison Memorial",
         "opening_hours" : {
            "open_now" : true
         },
         "photos" : [
            {
               "height" : 4000,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/101198926962203348411\"\u003eQuân Phan Lê Minh\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAnwdW5lev_sHN7ENomXGSuXSISTmb3jvBTUXVUmY282TMoFgmDT1TI-2WFdWA4Rvkd7nXKYW7i20cMh_QEHfZm9c0NE_xBZHeanYGSakXUeoFsIz9_6bZKLHuRcPzRvb8EhCRPFjS4DPRMm1tPpBD9_ZqGhQJT5u8TjfsjXzjHUejYJ77bOyTlA",
               "width" : 6000
            }
         ],
         "place_id" : "ChIJld5RqparNTERVK8x7gvhAZc",
         "plus_code" : {
            "compound_code" : "2RGW+3H Trần Hưng Đạo, Hoàn Kiếm, Hanoi",
            "global_code" : "7PH72RGW+3H"
         },
         "rating" : 4.3,
         "reference" : "ChIJld5RqparNTERVK8x7gvhAZc",
         "types" : [ "tourist_attraction", "point_of_interest", "establishment" ],
         "user_ratings_total" : 6698
      },
      {
         "formatted_address" : "Đồng Xuân, Hoàn Kiếm, Hanoi 100000, Vietnam",
         "geometry" : {
            "location" : {
               "lat" : 21.0382239,
               "lng" : 105.8495951
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 21.03968302989272,
                  "lng" : 105.8511645
               },
               "southwest" : {
                  "lat" : 21.03698337010728,
                  "lng" : 105.8481173
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/shopping-71.png",
         "id" : "dcf4ce5dfb0c12a7f52664c4456b32fee778a8a9",
         "name" : "Dong Xuan Market",
         "opening_hours" : {
            "open_now" : true
         },
         "photos" : [
            {
               "height" : 2560,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/117598661267546324260\"\u003eA Google User\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAm84sd3tJu4PXiSpAoyFhEzZhHir5y3iGp0DVcbTDE9iKbMZ5fb6s8PiuqDKUC1avferh21blWYfJWwxnhMrIvwgv-QUywWbOZIkvRhKYMqbUwTX-3U21_SsBNsP-7EQzEhAUX-bhoJTQEPW6jsfKB4M9GhQHfea_45JOeMOMphlRl_4Qs_rYwg",
               "width" : 1440
            }
         ],
         "place_id" : "ChIJ00--4birNTERAe3KhQgps3o",
         "plus_code" : {
            "compound_code" : "2RQX+7R Hanoi, Hoàn Kiếm, Hanoi",
            "global_code" : "7PH72RQX+7R"
         },
         "rating" : 4,
         "reference" : "ChIJ00--4birNTERAe3KhQgps3o",
         "types" : [ "tourist_attraction", "point_of_interest", "establishment" ],
         "user_ratings_total" : 15130
      },
      {
         "formatted_address" : "Chùa Một Cột, Đội Cấn, Ba Đình, Hà Nội 100000, Vietnam",
         "geometry" : {
            "location" : {
               "lat" : 21.0358565,
               "lng" : 105.8336184
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 21.03696917989272,
                  "lng" : 105.8350776798927
               },
               "southwest" : {
                  "lat" : 21.03426952010728,
                  "lng" : 105.8323780201073
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png",
         "id" : "403a6b314391a24c52f077b9751a80bd6b791f33",
         "name" : "One Pillar Pagoda",
         "opening_hours" : {
            "open_now" : true
         },
         "photos" : [
            {
               "height" : 1280,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/115823434009505732024\"\u003eA Google User\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAaq1Dsmfh0-bvcNS_BWmLr21-kUACH6yL8gRSxDYckFLtC491fBCv2a-VLCkU_c58iQn7PnCaN0p27o9BvBZeDYeSmJdHaaD1WBDRFQpHipKNxqDz5b13fcUU_cbaN0ukEhC11GXnvZGwaL1Z4jnqEm_RGhQpQOibMt14lV0MQLl-oMa9yiJ8Zw",
               "width" : 1920
            }
         ],
         "place_id" : "ChIJ7XWEcqGrNTERrsLf6W8259s",
         "plus_code" : {
            "compound_code" : "2RPM+8C Ba Đình, Hanoi",
            "global_code" : "7PH72RPM+8C"
         },
         "rating" : 4.3,
         "reference" : "ChIJ7XWEcqGrNTERrsLf6W8259s",
         "types" : [
            "tourist_attraction",
            "place_of_worship",
            "point_of_interest",
            "establishment"
         ],
         "user_ratings_total" : 5368
      },
      {
         "formatted_address" : "Thanh Niên, Yên Phụ, Tây Hồ, Hà Nội 100000, Vietnam",
         "geometry" : {
            "location" : {
               "lat" : 21.0479549,
               "lng" : 105.8369399
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 21.04941542989272,
                  "lng" : 105.8387927298927
               },
               "southwest" : {
                  "lat" : 21.04671577010728,
                  "lng" : 105.8360930701073
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png",
         "id" : "1c465fe7f47c703ff99521f08b4d03b958bb0612",
         "name" : "Tran Quoc Pagoda",
         "opening_hours" : {
            "open_now" : true
         },
         "photos" : [
            {
               "height" : 3024,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/112492983229803783356\"\u003eyoungju Hong\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAA35qDPSAE621cA-kOTYXwd0SNXL5TvwQN3Kkp3MIWP_O-AEw5im9fn65_gKG8LP-Ck-LUeUhnGe-G4K2NESEsY_AtaQgj3CyIstsjC5g9v9C-pVDtRntslmBy0-yVYsUFEhBTt8zKPOv390OGrmz02lu9GhTGQ1My2rblOvTgeKY6W9td6hYJ0A",
               "width" : 4032
            }
         ],
         "place_id" : "ChIJr7enHa-rNTERbivpCTqoenY",
         "plus_code" : {
            "compound_code" : "2RXP+5Q Yên Phụ, Tây Hồ, Hanoi",
            "global_code" : "7PH72RXP+5Q"
         },
         "rating" : 4.4,
         "reference" : "ChIJr7enHa-rNTERbivpCTqoenY",
         "types" : [
            "tourist_attraction",
            "place_of_worship",
            "point_of_interest",
            "establishment"
         ],
         "user_ratings_total" : 7964
      },
      {
         "formatted_address" : "87 Phố Mã Mây, Hàng Buồm, Hoàn Kiếm, Hà Nội, Vietnam",
         "geometry" : {
            "location" : {
               "lat" : 21.0345585,
               "lng" : 105.8535189
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 21.03583807989272,
                  "lng" : 105.8548129298927
               },
               "southwest" : {
                  "lat" : 21.03313842010728,
                  "lng" : 105.8521132701073
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png",
         "id" : "b0fc493251af54397a808e55ee30a5e443f7d047",
         "name" : "Hanoi Ancient House",
         "opening_hours" : {
            "open_now" : true
         },
         "photos" : [
            {
               "height" : 3096,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/116576492996775634641\"\u003eQuang Nguyễn\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAqR4L8VJom2XJjY_wUl1iqssNK0MZFG2AN6mqSaN3Si34P78jlfXR8u_GxcGmCObMZMoWMffbA1W78TuDuWmU03Jwqlpwm7gsIy3gbrlYBZNt7bNqROHUWxMJHmgE6d8JEhCPswCOOqd9OFA2Rqhnq6OhGhQ59SX48DL5JRAXCr-B2dWFDFAK3A",
               "width" : 4128
            }
         ],
         "place_id" : "ChIJRbyjlsCrNTERE429Nn3MHes",
         "plus_code" : {
            "compound_code" : "2VM3+RC Hanoi, Hoàn Kiếm, Hanoi",
            "global_code" : "7PH72VM3+RC"
         },
         "rating" : 4.2,
         "reference" : "ChIJRbyjlsCrNTERE429Nn3MHes",
         "types" : [ "tourist_attraction", "point_of_interest", "establishment" ],
         "user_ratings_total" : 626
      },
      {
         "formatted_address" : "Nguyễn Văn Huyên, Quan Hoa, Cầu Giấy, Hà Nội 100000, Vietnam",
         "geometry" : {
            "location" : {
               "lat" : 21.040633,
               "lng" : 105.7985155
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 21.04198647989272,
                  "lng" : 105.7994440798927
               },
               "southwest" : {
                  "lat" : 21.03928682010728,
                  "lng" : 105.7967444201073
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/museum-71.png",
         "id" : "777f14b13038906bec5e4e3fb6b17da6396d2a01",
         "name" : "Vietnam Museum Of Ethnology",
         "opening_hours" : {
            "open_now" : true
         },
         "photos" : [
            {
               "height" : 4128,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/115975998380102625104\"\u003eCrystal Alexander\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAALU9dg2308bF_QoQ9E2vxrRNJ40jCMHoHtsbGpRcyjTFO2g3uzQs1r2bI6ZZPMj2AhZNiIK3_daVgpYf7ItOSzf-p0EHckXDpRSyPa1WHHJRjwvAwLp_7b8dvtH70LzDAEhAyYjpDV4ZUbnP4ZmObPvJ0GhQgk4d_brBDYFG1y2_eyiuntzbWwA",
               "width" : 3096
            }
         ],
         "place_id" : "ChIJIbhTQzmrNTERHBoID7tQyUc",
         "plus_code" : {
            "compound_code" : "2QRX+7C Quan Hoa, Cầu Giấy, Hanoi",
            "global_code" : "7PH72QRX+7C"
         },
         "rating" : 4.5,
         "reference" : "ChIJIbhTQzmrNTERHBoID7tQyUc",
         "types" : [ "tourist_attraction", "museum", "point_of_interest", "establishment" ],
         "user_ratings_total" : 6961
      },
      {
         "formatted_address" : "76 Hàng Buồm, Hoàn Kiếm, Hà Nội, Vietnam",
         "geometry" : {
            "location" : {
               "lat" : 21.0357895,
               "lng" : 105.8510112
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 21.03708187989272,
                  "lng" : 105.8523806798927
               },
               "southwest" : {
                  "lat" : 21.03438222010728,
                  "lng" : 105.8496810201073
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png",
         "id" : "3c821b15edf4ef38f638fa4ce98640b8c3ca0b4f",
         "name" : "Bach Ma Temple",
         "opening_hours" : {
            "open_now" : true
         },
         "photos" : [
            {
               "height" : 2976,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/113589704458383213727\"\u003eG L Littleton\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAJjCSClw2nlx_hON0mRIZF5oBFDJfCEcnCVf9-8743QRZDJY6SL5fFcBrdBv_PJKc4zKZBOvSNuVhPrSmouIumldBps60iV92FmsJW17VrXWDRm4XdpIVjZsyktwmUd3LEhAWMnMZHYdS3tFUFM_q16SmGhR7Xj6m_QNqLvy_andJPgp4EL8l6g",
               "width" : 3968
            }
         ],
         "place_id" : "ChIJ9Y3KR7-rNTERawQ7FeFv5gA",
         "plus_code" : {
            "compound_code" : "2VP2+8C Hàng Buồm, Hoàn Kiếm, Hanoi",
            "global_code" : "7PH72VP2+8C"
         },
         "rating" : 4.3,
         "reference" : "ChIJ9Y3KR7-rNTERawQ7FeFv5gA",
         "types" : [
            "tourist_attraction",
            "place_of_worship",
            "point_of_interest",
            "establishment"
         ],
         "user_ratings_total" : 332
      },
      {
         "formatted_address" : "2 Hùng Vương, Điện Bàn, Ba Đình, Hà Nội 100000, Vietnam",
         "geometry" : {
            "location" : {
               "lat" : 21.0367789,
               "lng" : 105.8346447
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 21.0384932,
                  "lng" : 105.8362697798927
               },
               "southwest" : {
                  "lat" : 21.0349244,
                  "lng" : 105.8335701201073
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png",
         "id" : "a9cce65a5f7d83f424928dea35bf053037f95116",
         "name" : "Ho Chi Minh Mausoleum",
         "opening_hours" : {
            "open_now" : false
         },
         "photos" : [
            {
               "height" : 4000,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/106929495006038503888\"\u003eMinh Ngo Van\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAZWpXZ_MroHxKS1bdHzibZlK0741vV9JlXuIMiaJVW0uTwpbcigdDcSQsgJhvI3GOfOpOBo1bHdJkf2ZPy2XHnFJImShQxQXiFJqEQ71qtg0rB1Lsk3TiAaVO4TlqU6xWEhBH7nTJYNC4MTa7EMTzOuaoGhQp8itdZ7SvkiBdANZ9YKhGHOkCgg",
               "width" : 6000
            }
         ],
         "place_id" : "ChIJF13BXqGrNTERTE3hz8KFDmI",
         "plus_code" : {
            "compound_code" : "2RPM+PV Điện Bàn, Ba Đình, Hanoi",
            "global_code" : "7PH72RPM+PV"
         },
         "rating" : 4.5,
         "reference" : "ChIJF13BXqGrNTERTE3hz8KFDmI",
         "types" : [
            "tourist_attraction",
            "general_contractor",
            "point_of_interest",
            "establishment"
         ],
         "user_ratings_total" : 13425
      },
      {
         "formatted_address" : "Hàng Trống, Hoàn Kiếm, Hanoi, Vietnam",
         "geometry" : {
            "location" : {
               "lat" : 21.0278588,
               "lng" : 105.8522899
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 21.02920862989272,
                  "lng" : 105.8536397298927
               },
               "southwest" : {
                  "lat" : 21.02650897010728,
                  "lng" : 105.8509400701073
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png",
         "id" : "5123ed1bd56713a0c9fab496f9ece5da0ed6ff44",
         "name" : "Hanoikids Free Tour",
         "opening_hours" : {
            "open_now" : true
         },
         "photos" : [
            {
               "height" : 1536,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/106357026203654367349\"\u003eA Google User\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAA5Jkp6PxGcNnFzep9s1cwbhGwwR4Ll4QrJCKKwNINabuuS8UbSDN620JtbuATJlOG1zY1uFTbzfzE2EEVmL_ipqew28GnLvpoJRX9PTrxmaGvrLAXTquV_BcexB9qG5RzEhA1lS4UhnElOSgr5YbJZVfXGhR4A_7qlibQ_ayeC5uyiTV3trT33g",
               "width" : 2048
            }
         ],
         "place_id" : "ChIJQTzcM9yrNTERLxcPs7bhjXA",
         "plus_code" : {
            "compound_code" : "2VH2+4W Hoàn Kiếm, Hanoi",
            "global_code" : "7PH72VH2+4W"
         },
         "rating" : 4.4,
         "reference" : "ChIJQTzcM9yrNTERLxcPs7bhjXA",
         "types" : [ "tourist_attraction", "point_of_interest", "establishment" ],
         "user_ratings_total" : 27
      },
      {
         "formatted_address" : "Số 1 Ngõ Bách Thảo, Ngọc Hồ, Ba Đình, Hà Nội 118322, Vietnam",
         "geometry" : {
            "location" : {
               "lat" : 21.0381439,
               "lng" : 105.8331508
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 21.03948142989272,
                  "lng" : 105.8345161298927
               },
               "southwest" : {
                  "lat" : 21.03678177010728,
                  "lng" : 105.8318164701073
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png",
         "id" : "ecf0f51474679b4022268921f26ef35a9ac1e182",
         "name" : "Ho Chi Minh’s Stilt House",
         "opening_hours" : {
            "open_now" : true
         },
         "photos" : [
            {
               "height" : 666,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/100083426681789471877\"\u003ethang dao\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAVkjSpi3iASRr3PDJXSHjRtGrWZwfb36dwnKkBYa9CL3r5Pgv7ebUuLem-PJfoqUk3Nyk69pgubbUxuDK38eRLREpTjyS3f_-IvaNT8SRsKu6zrNaimPT_aOQI1TgICEEEhCf3H7CRkQYf-J4IgTbBQwwGhRrw87J3xpCc0wWqk2JuRKrSvlKTQ",
               "width" : 1000
            }
         ],
         "place_id" : "ChIJm5BAy6arNTERO1z1B3z-0BU",
         "plus_code" : {
            "compound_code" : "2RQM+77 Ngọc Hồ, Ba Đình, Hanoi",
            "global_code" : "7PH72RQM+77"
         },
         "rating" : 4.4,
         "reference" : "ChIJm5BAy6arNTERO1z1B3z-0BU",
         "types" : [ "tourist_attraction", "point_of_interest", "establishment" ],
         "user_ratings_total" : 844
      },
      {
         "formatted_address" : "40 Nhà Chung, Hàng Trống, Hoàn Kiếm, Hà Nội, Vietnam",
         "geometry" : {
            "location" : {
               "lat" : 21.0286657,
               "lng" : 105.8488123
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 21.03022472989272,
                  "lng" : 105.8505084298927
               },
               "southwest" : {
                  "lat" : 21.02752507010728,
                  "lng" : 105.8478087701073
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/worship_general-71.png",
         "id" : "582996a727a5988b32d390f8475e38eae2725d49",
         "name" : "St. Joseph's Cathedral",
         "opening_hours" : {
            "open_now" : true
         },
         "photos" : [
            {
               "height" : 1536,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/117818300376102779069\"\u003eNgo\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAATllwKAAGppHrsyD_2DOeFKRLmQ5UYz_E4zUzcao5umOQed3-97dLNovjmahwXATvaFM26O69QbUA-DsTSbYy_z0o2l9abf_5qkR4wh7wQlCbHQoTZCRtCDDLppGAcpfnEhCtvyVC-9rSNXJmCXcPPK-LGhT4lqON0YiBwf0TRRifiy8PEe5Sjg",
               "width" : 2048
            }
         ],
         "place_id" : "ChIJS7GypZWrNTERr2jliTrPBTI",
         "plus_code" : {
            "compound_code" : "2RHX+FG Hàng Trống, Hoàn Kiếm, Hanoi",
            "global_code" : "7PH72RHX+FG"
         },
         "rating" : 4.5,
         "reference" : "ChIJS7GypZWrNTERr2jliTrPBTI",
         "types" : [
            "tourist_attraction",
            "church",
            "place_of_worship",
            "point_of_interest",
            "establishment"
         ],
         "user_ratings_total" : 12137
      },
      {
         "formatted_address" : "28A Điện Biên Phủ, Điện Bàn, Ba Đình, Hà Nội, Vietnam",
         "geometry" : {
            "location" : {
               "lat" : 21.0325556,
               "lng" : 105.8398039
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 21.03387852989272,
                  "lng" : 105.8409097298927
               },
               "southwest" : {
                  "lat" : 21.03117887010728,
                  "lng" : 105.8382100701073
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png",
         "id" : "63066807b54422afebd0f60f9565508cad71d4f8",
         "name" : "Hanoi Flagtower",
         "opening_hours" : {
            "open_now" : true
         },
         "photos" : [
            {
               "height" : 2736,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/100650801838554449109\"\u003ePattarawan Prayong\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAvP8xh2sVs5kPWYKQHe3y4kMnK1AZbN_sFyyuAe0GMId_x7MlqLMqdLI047bDU-h2YCdLOrgkeCzPVq8paWvCmA2dFLAYicPCklwp2b8q7itQmQbnHPaVi0XxVmnY8OBBEhAZ7TeQMbuHLqw7Q7nJVPC9GhRA-zZoKUDSfwTRWmh9suHS0RYSfQ",
               "width" : 3648
            }
         ],
         "place_id" : "ChIJxZVbuqKrNTER4I2XSRfxbLg",
         "plus_code" : {
            "compound_code" : "2RMQ+2W Hanoi, Hoàn Kiếm, Hanoi",
            "global_code" : "7PH72RMQ+2W"
         },
         "rating" : 4.5,
         "reference" : "ChIJxZVbuqKrNTER4I2XSRfxbLg",
         "types" : [ "tourist_attraction", "point_of_interest", "establishment" ],
         "user_ratings_total" : 795
      },
      {
         "formatted_address" : "Trần Phú, Hàng Bông, Hoàn Kiếm, Hà Nội 100000, Vietnam",
         "geometry" : {
            "location" : {
               "lat" : 21.030098,
               "lng" : 105.8440237
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 21.03142447989272,
                  "lng" : 105.8453673298927
               },
               "southwest" : {
                  "lat" : 21.02872482010727,
                  "lng" : 105.8426676701073
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png",
         "id" : "abd8d285fce0ddd8e1d6718e7e302bd1919390a4",
         "name" : "Hanoi Train Street",
         "opening_hours" : {
            "open_now" : true
         },
         "photos" : [
            {
               "height" : 3265,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/107478255631936414951\"\u003eCheng-Hung Lin\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAm5h5dyVLXZUTUmS_e9VAKGgu4XwAghmMXDIqmLbQum1ifELleF5jxJAsZ3Yg2POAkk8SVoLJM9inCm8VcZBNl-hGB4JVOn8-R4_18Zh4f1IJLu3-Uv5okWVjl6-Q_da0EhAg66ZHa4YEXWLJ-FTVw4XCGhQtfE5vT623TkgWKVPLFW2gxvkK1g",
               "width" : 4898
            }
         ],
         "place_id" : "ChIJiYIaNJarNTERGWzmm8msKRg",
         "plus_code" : {
            "compound_code" : "2RJV+2J Hàng Bông, Hoàn Kiếm, Hanoi",
            "global_code" : "7PH72RJV+2J"
         },
         "rating" : 4.4,
         "reference" : "ChIJiYIaNJarNTERGWzmm8msKRg",
         "types" : [ "tourist_attraction", "point_of_interest", "establishment" ],
         "user_ratings_total" : 5811
      },
      {
         "formatted_address" : "28A Điện Biên Phủ, Điện Bàn, Ba Đình, Hà Nội, Vietnam",
         "geometry" : {
            "location" : {
               "lat" : 21.0322412,
               "lng" : 105.8402963
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 21.03334687989272,
                  "lng" : 105.8415161298927
               },
               "southwest" : {
                  "lat" : 21.03064722010728,
                  "lng" : 105.8388164701073
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/museum-71.png",
         "id" : "c44e7225676187a45d1fff790d12b31b5c60b015",
         "name" : "Vietnam Military History Museum",
         "opening_hours" : {
            "open_now" : true
         },
         "photos" : [
            {
               "height" : 3024,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/101783593755845417656\"\u003eNelly Nguyen\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAApJfQ9GIvKqAFD6tO6_iaWJSwFBYJanHnG2ov9VcDCXqYr6BkeQqDRUQnN1cQ4AdSwsIKosBG2QKF9s2Fknq3lJdTJcg6IJzJPpTvQZGf16qlY80m2TjpGgSWPKkZeuLYEhD1K288fiYIe0dvOBCmtQ99GhTHJQgYoSErqcCLmH9TE6HvMu0-hA",
               "width" : 4032
            }
         ],
         "place_id" : "ChIJE109t6KrNTERsaAoYzbvoKY",
         "plus_code" : {
            "compound_code" : "2RJR+V4 Điện Bàn, Ba Đình, Hanoi",
            "global_code" : "7PH72RJR+V4"
         },
         "rating" : 4.2,
         "reference" : "ChIJE109t6KrNTERsaAoYzbvoKY",
         "types" : [ "tourist_attraction", "museum", "point_of_interest", "establishment" ],
         "user_ratings_total" : 3802
      },
      {
         "formatted_address" : "Hồng Hà, Phúc Tân, Hoàn Kiếm, Hà Nội, Vietnam",
         "geometry" : {
            "location" : {
               "lat" : 21.0368682,
               "lng" : 105.853825
            },
            "viewport" : {
               "northeast" : {
                  "lat" : 21.03823742989272,
                  "lng" : 105.8552013798927
               },
               "southwest" : {
                  "lat" : 21.03553777010728,
                  "lng" : 105.8525017201073
               }
            }
         },
         "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png",
         "id" : "894e61ee47ce27191c5f62673867a7435b7faa8c",
         "name" : "Ceramic Mosaic Mural",
         "opening_hours" : {
            "open_now" : true
         },
         "photos" : [
            {
               "height" : 1365,
               "html_attributions" : [
                  "\u003ca href=\"https://maps.google.com/maps/contrib/115172098895115282390\"\u003eKokoy Severino\u003c/a\u003e"
               ],
               "photo_reference" : "CmRaAAAAsNrtUiVuicyhwVCgwxknZqDl80C_fTw37l8cHqhC3gVUbiM9aapaTr_eov5b_b21SnCLZWedtGqDDR-oJGX0B6TItvYzXijlYioDIfEvumTzqThcuhpQ8wmx3yHmOcyvEhDMBs7IFCt7WnXSZD5wYXsMGhS6Tqlf41oUtwO2G-HDkHTjyAmXMg",
               "width" : 2048
            }
         ],
         "place_id" : "ChIJ-0nUGLGrNTERfZ9bDCZ56JE",
         "plus_code" : {
            "compound_code" : "2VP3+PG Phúc Tân, Hoàn Kiếm, Hanoi",
            "global_code" : "7PH72VP3+PG"
         },
         "rating" : 4.2,
         "reference" : "ChIJ-0nUGLGrNTERfZ9bDCZ56JE",
         "types" : [ "tourist_attraction", "point_of_interest", "establishment" ],
         "user_ratings_total" : 452
      }
   ],
   "status" : "OK"
};

  fetchWeather = () => {
    fetch('http://ip-api.com/json')
      .then(res => res.json())
      .then(json => {
        console.log(json);
        this.setState({ location: json.city });
        this.getPlaces(json.city);
        this.setState({ lat: json.lat});
        this.setState({ lat: json.lon});
      });

    fetch('https://api.openweathermap.org/data/2.5/weather?lat=21.0313&lon=105.8516&appid=90b145f0767c7ff5ee86f5ef0e958fce')
      .then(res => res.json())
      .then(json => {
        console.log(json);
        this.setState({ temperature: Math.floor(json.main.temp - 273) });
        this.setState({ humidity: json.main.humidity });
        this.setState({ description: json.weather[0].description });
      });


        // this.setState({ place1: this.dataPlaces.results[0].name });
        // this.setState({ place2: this.dataPlaces.results[1].name });
        // this.setState({ place3: this.dataPlaces.results[2].name });
        // this.setState({ place4: this.dataPlaces.results[3].name });
        // this.setState({ place5: this.dataPlaces.results[4].name });

  }

  getPlaces = (location) => {
    console.log('https://maps.googleapis.com/maps/api/place/textsearch/json?query='+location+'+city+point+of+interest&language=vn&key=AIzaSyCHvGqrDVxQu7k_zlshdP7OCDCkpin3Twk');
    fetch(
      'https://maps.googleapis.com/maps/api/place/textsearch/json?query=${location}+city+point+of+interest&language=vn&key=AIzaSyCHvGqrDVxQu7k_zlshdP7OCDCkpin3Twk'
    )
      .then(res => res.json())
      .then(json => {
        console.log(json);
        this.setState({ place1: json.results[0].name });
        this.setState({ place2: json.results[1].name });
        this.setState({ place3: json.results[2].name });
        this.setState({ place4: json.results[3].name });
        this.setState({ place5: json.results[4].name });
        this.setState({ placeAddress1: json.results[0].formatted_address });
        this.setState({ placeAddress2: json.results[1].formatted_address });
        this.setState({ placeAddress3: json.results[2].formatted_address });
        this.setState({ placeAddress4: json.results[3].formatted_address });
        this.setState({ placeAddress5: json.results[4].formatted_address });
      });
  }

  componentDidMount(){
   this.fetchWeather();
  }
  


  render() {
    return (
      <ImageBackground
        style={styles.backgroundImage}
        source={require('./background.jpg')}
      >
        <View style={styles.header}>
            <View>
              <Text
                style={styles.location}
              >
                {this.state.location}
              </Text>
              <Text
                style={styles.discription}
              >
                {this.state.description}
              </Text>
              <Text
                style={styles.temperature} onPress={this.fetchWeather}
              >
                {this.state.temperature}°C
              </Text>
              <Text
                style={styles.humidity}
              >
                Humidity: {this.state.humidity}%
              </Text>
            </View>
            <ImageBackground style={styles.imageSuny} source={require('./weather02-512.png')}>
            </ImageBackground>
        </View>
        <View style={styles.more}>
            <ImageBackground style={styles.goiy} source={require('./goi_y.png')}></ImageBackground>
            <Text style={styles.textGray}>Recent tourist attractions</Text>
        </View>
        <ScrollView>
        <View style={styles.places}>
            <View style={styles.place}>
                <View style={styles.row}>
                    <ImageBackground style={styles.iconPlace} source={require('./1527531.png')}></ImageBackground>
                    <View style={styles.column}>
                        <Text style={styles.name}>{this.state.place1}</Text>
                        <Text>Address: {this.state.placeAddress1}</Text>
                    </View>
                </View>
            </View>
            <View style={styles.place}>
                <View style={styles.row}>
                    <ImageBackground style={styles.iconPlace} source={require('./1527531.png')}></ImageBackground>
                    <View style={styles.column}>
                        <Text style={styles.name}>{this.state.place2}</Text>
                        <Text>Address: {this.state.placeAddress2}</Text>
                    </View>
                </View>
            </View>
            <View style={styles.place}>
                <View style={styles.row}>
                    <ImageBackground style={styles.iconPlace} source={require('./1527531.png')}></ImageBackground>
                    <View style={styles.column}>
                        <Text style={styles.name}>{this.state.place3}</Text>
                        <Text>Address: {this.state.placeAddress3}</Text>
                    </View>
                </View>
            </View>
            <View style={styles.place}>
                <View style={styles.row}>
                    <ImageBackground style={styles.iconPlace} source={require('./1527531.png')}></ImageBackground>
                    <View style={styles.column}>
                        <Text style={styles.name}>{this.state.place4}</Text>
                        <Text>Address: {this.state.placeAddress4}</Text>
                    </View>
                </View>
            </View>
            <View style={styles.place}>
                <View style={styles.row}>
                    <ImageBackground style={styles.iconPlace} source={require('./1527531.png')}></ImageBackground>
                    <View style={styles.column}>
                        <Text style={styles.name}>{this.state.place5}</Text>
                        <Text>Address: {this.state.placeAddress5}</Text>
                    </View>
                </View>
            </View>
        </View>
        </ScrollView>
      </ImageBackground>
    );
  }
}



const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  header: {
    padding: 20,
    flexDirection: 'row',
    margin: 15,
    borderRadius: 20,
    backgroundColor: 'rgba(255, 255, 255, 0.5)',
  },
  more: {
    flexDirection: 'row',
    borderRadius: 20,
    margin: 15,
    padding: 10,
    paddingLeft: 20,
    backgroundColor: 'rgba(255, 255, 255, 0.5)',
    color: 'gray',
  },
  goiy: {
    width: 20,
    height: 20,
    paddingRight: 30,
  },
  textGray: {
    color: 'gray',
  },
  places: {
    overflow: 'scroll',
  },
  place: {
    flexDirection: 'column',
    margin: 15,
    marginTop: 3,
    marginBottom: 3,
    padding: 20,
    backgroundColor: 'rgba(255, 255, 255, 0.5)',
  },
  row: {
    flexDirection: 'row',
  },
  column: {
    paddingLeft: 20,
    paddingRight: 30,
    flexDirection: 'column',
  },
  iconPlace: {
    width: 50,
    height: 50,
  },
  name: {
    fontWeight: 'bold',
  },
  imageSuny: {
    left: 50,
    padding: 20,
    width: 150,
    height: 150,
  },
  location: {
    textAlign: 'left',
    fontSize: 30,
    color: 'gray',
  },
  temperature: {
    textAlign: 'left',
    fontSize: 50,
    fontWeight: 'normal',
    color: 'gray',
  },
  humidity: {
    fontSize: 20,
    color: 'gray',
  },
  discription: {
    textAlign: 'left',
    fontSize: 15,
    color: 'gray',
  }
})

AppRegistry.registerComponent('BackgroundImage', () => BackgroundImage);